<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Authenticate;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\QualificationController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'login'])->name('admin_login');
Route::get('/', [AuthController::class, 'logout'])->name('logout');
Route::post('/', [AuthController::class, 'authenticate'])->name('admin');


Route::middleware([Authenticate::class])->group(function () {
    Route::resource('User', Usercontroller::class);
    Route::resource('Student', Studentcontroller::class);
    Route::resource('Course', Coursecontroller::class);
    Route::resource('Qualification', QualificationController::class);
    Route::get('/inicio', [HomeController::class, 'index'])->name('index');  
    
});


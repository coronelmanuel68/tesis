<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rol;
use App\Models\Person;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $listUsers=User::with('people','rol')->get();
        return view(('User.list'), compact('listUsers'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Rol::all();
        $user=new User;
        return view(('User.create'), compact('roles','user'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'dni'        => 'required',
            'birthdate'  => 'required',
            'email'      => 'required|unique:users,email',
            'password'    => 'required',
            'rol'        => 'required',
        ], [
            'name.required'       => 'name',
            'lastname.required'   => 'lastname',
            'dni.required'        => 'dni',
            'birthdate.required'  => 'birthdate',
            'email.unique'        => 'email',
            'password.required'    => 'password',
            'rol.required'        => 'rol',
        ]);
        $people=new Person;
        $people->name= $request->name;
        $people->lastname= $request->lastname;
        $people->dni= $request->dni;
        $people->birthdate= $request->birthdate;
        $people->save();

        $user=new User;
        $user->email= $request->email;
        $user->password=Hash::make($request->password);
        $user->rol_id= $request->rol;
        $user->people_id= $people->id;
        $user->save();

        return redirect('User/create')
                ->with('typemsg','success')
                ->with('message','Ususario creado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user   = User::with('people')->findOrFail($id);
        $roles  = Rol::all();
       
        return view(('User.update'), compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'dni'        => 'required',
            'birthdate'  => 'required',
            'email'      => 'unique:users,email',
            'rol'        => 'required',
        ], [
            'name.required'       => 'name',
            'lastname.required'   => 'lastname',
            'dni.required'        => 'dni',
            'birthdate.required'  => 'birthdate',
            'email.unique'        => 'email',
            'rol.required'        => 'rol',
        ]);

        $user = User::findOrFail($id);
        if (!is_null($request->email)) {
            $user->email      = $request->email;
        }
        if (!is_null($request->password)) {
            $user->password   = Hash::make($request->password);
        }
        $user->rol_id     = $request->rol;


        $people = Person::findOrFail($user->people_id);
     
        $people->name         = $request->name;
        $people->lastname     = $request->lastname;
        $people->dni          = $request->dni;
        $people->birthdate    = $request->birthdate;
        $people->save(); 
            
        if($people ->save()){
            return redirect('User')
                ->with('typemsg', 'success')
                ->with('message', 'Usuario se modifico correctamente.');
        }else{
            return redirect('User')
                ->with('typemsg', 'error')
                ->with('message', 'Upps hubo un problema al guardar el Usuario.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $people = Person::find($user->people_id);
        $user->delete();
        $people->delete();
        return redirect('User')
        ->with('typemsg', 'success')
        ->with('message', 'El ususario se elimino correctamente');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
       dd('estoy en login');
        return view('login');
    }
    
    public function authenticate(Request $request)
    {
       
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
            return redirect('/inicio');
        }
        else {
            return redirect('/admin')
                ->with('error','Usuario o password incorrectos.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return view('login');
    }   
}

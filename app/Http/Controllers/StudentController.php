<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Person;
use App\Models\State;
use App\Models\Course;
use App\Models\TypeOfContact;
use App\Models\Contact;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $listStudent=Student::with('people','state','courses')->get();
       
        return view(('Student.list'), compact('listStudent'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typeOfContacts=TypeOfContact::all();
        $courses=Course::all();
        $student=new Student;
        return view(('Student.create'), compact('typeOfContacts','student','courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'dni'        => 'required',
            'birthdate'  => 'required',
            'contact'    => 'required',
            'course'    => 'required',
            
        ], [
            'name.required'       => 'name',
            'lastname.required'   => 'lastname',
            'dni.required'        => 'dni',
            'birthdate.required'  => 'birthdate',
            'contact.unique'      => 'contact',
            'course.required'     => 'course',
            
        ]);
       
        $people=new Person;
        $people->name= $request->name;
        $people->lastname= $request->lastname;
        $people->dni= $request->dni;
        $people->birthdate= $request->birthdate;
        $people->save();

        $contact=new Contact;
        $contact->contact= $request->contact;
        $contact->people_id= $people->id;
        $contact->type_of_contact_id= $request->type_of_contact;
        $contact->save();

        $student=new Student;
        $student->state_id=1;
        $student->courses_id= $request->course;
        $student->people_id= $people->id;
        $student->save();
      

        return redirect('Student/create')
                ->with('typemsg','success')
                ->with('message','Estudiante creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student  = Student::with('people','courses')->findOrFail($id);
        $courses  = Course::all();
        $contact  = Contact::where('people_id','=',$student->people->id)->first();
        $typeOfContacts=TypeOfContact::all();
        
        $student = new Request([
            'id'=> $student->id,
            'name'=> $student->people->name,
            'lastname'=> $student->people->lastname,
            'dni'=> $student->people->dni,
            'birthdate'=> $student->people->birthdate,
            'course'=> $student->courses_id,
            'contact'=> $contact->contact,
            'type_of_contact'=> $contact->type_of_contact_id,
        ]);
       
        
        return view(('Student.update'), compact('student','courses','typeOfContacts'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'dni'        => 'required',
            'birthdate'  => 'required',
            'contact'    => 'required',
            
        ], [
            'name.required'       => 'name',
            'lastname.required'   => 'lastname',
            'dni.required'        => 'dni',
            'birthdate.required'  => 'birthdate',
            'contact.required'    => 'contact',
           
            ]);
        

        $student= Student::findOrFail($id);
       
        $student->state_id=1;
        $student->courses_id = $request->courses;
        $student->save();
            
        $people = Person::findOrFail($student->people_id);
        $people->name         = $request->name;
        $people->lastname     = $request->lastname;
        $people->dni          = $request->dni;
        $people->birthdate    = $request->birthdate;
        $people->save();

        $contacts= Contact::where('people_id',$student->people_id)->get();
       
        foreach ($contacts as $contact) {
            $contact->contact= $request->contact;
            $contact->type_of_contact_id= $request->type_of_contact;
            $contact->save();
        }
            
        if($contact->save()){
            return redirect('Student')
                ->with('typemsg', 'success')
                ->with('message', 'Usuario se modifico correctamente.');
        }else{
            return redirect('Student')
                ->with('typemsg', 'error')
                ->with('message', 'Upps hubo un problema al guardar el Usuario.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $student = Student::find($id);
        $people = Person::find($people_id);
        $contact = Contact::find($contact->people_id);
        $student->delete();
        $people->delete();
        $contact->delete();
        return redirect('Student')
        ->with('typemsg', 'success')
        ->with('message', 'El ususario se elimino correctamente');
    }
}

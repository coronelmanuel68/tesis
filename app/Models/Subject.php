<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    public function course()
    {
         return $this->belongsTo('App\Models\Course');

    }

    public function teacher()
    
    {
         return $this->belongsToMany('App\Models\Teacher');
    
    }
    
    public function student()
    
    {
         return $this->belongsToMany('App\Models\Student');
    
    }

    public function qualification()
    {
         return $this->hasMany('App\Models\Qualification');
    }

    public function attendance()
    {
         return $this->hasMany('App\Models\Attendance');
    }
}

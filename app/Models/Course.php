<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    


    public function subject()
    {
         return $this->hasMany('App\Models\Subcjet');
    }

    public function student()
    {
         return $this->hasMany('App\Models\Student');
    }
}

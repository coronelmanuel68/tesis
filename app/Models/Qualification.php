<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    use HasFactory;
    
    
    public function student()
    
    {
         return $this->belongsTo('App\Models\Student');
    }

    public function subject()
    {
         return $this->belongsTo('App\Models\Subject');
    }
}

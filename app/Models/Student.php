<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    
    
    public function people()
    {
         return $this->belongsTo('App\Models\Person');
    }

    public function state()
    {
         return $this->belongsTo('App\Models\State');

    }

    public function qualification()
    {
         return $this->hasMany('App\Models\Qualification');
     }

     public function courses()
    {
         return $this->belongsTo('App\Models\Course');

    }

    public function subject()
    
    {
         return $this->belongsToMany('App\Models\Subject');
    
    }

}

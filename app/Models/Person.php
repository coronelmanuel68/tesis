<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    public function student()
    {
         return $this->hasMany('App\Models\Student');
    }


    public function contact()
    {
         return $this->hasMany('App\Models\Constact');
    }
    
    public function user()
    {
         return $this->hasMany('App\Models\User');
    }
    
    public function teacher()
    {
         return $this->hasMany('App\Models\Teacher');
    }

    public function attendance()
    {
         return $this->hasMany('App\Models\Attendance');
    }


    public function executive()
    {
         return $this->hasMany('App\Models\Executive');
    }
}

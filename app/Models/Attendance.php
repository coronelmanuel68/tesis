<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    public function people()
    {
         return $this->belongsTo('App\Models\Person');
    }

    public function subject()
    {
         return $this->belongTo('App\Models\Subject');
    }

}

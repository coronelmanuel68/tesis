<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="es">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>
        Gestion Estudiantil | Login
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
    WebFont.load({
        google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{asset('templante/metronic/default/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>
    <link href="{{asset('templante/metronic/default/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="../../../assets/demo/default/media/img/logo/favicon.ico" />
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
            <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside fondoLogin">
                <div class="m-stack m-stack--hor m-stack--desktop">
                    <div class="m-stack__item m-stack__item--fluid">
                        <div class="m-login__wrapper">
                            <div class="m-la-desktop ">
                                <a href="{{asset('templante/metronic/default/components/icons/lineawesome.html')}}">
                                    <img class="logoLogin" src="{{asset('templante/iconouser.png')}}">
                                </a>
                            </div>
                            <div class="m-login__signin">
                                <div class="m-login__head">
                                    <h3 class="m-login__title">
                                        Ingreso
                                    </h3>
                                </div>
                                <form class="m-login__form m-form" method="POST" action="{{route ('admin')}}">
                                    {{ csrf_field() }}
                                    @if (session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}">

                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Contraseña" name="password">
                                    </div>
                                    <!--<div class="row m-login__form-sub">
                                        <div class="col m--align-left">
                                            <label class="m-checkbox m-checkbox--focus">
                                                <input type="checkbox" name="remember"> Recordarme
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col m--align-right">
                                            <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                                    Olvidaste la contraseña?
                                            </a>
                                        </div>
                                    </div>-->
                                    <div class="m-login__form-action">
                                        <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                            Ingresar
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="m-login__signup">
                                <div class="m-login__head">
                                    <h3 class="m-login__title">
                                            Registrate
                                        </h3>
                                    <div class="m-login__desc">
                                        Ingrese su correo electrónico para restablecer su contraseña:
                                    </div>
                                </div>
                                <form class="m-login__form m-form" action="">
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" placeholder="Fullname" name="fullname">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="password" placeholder="Password" name="password">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpassword">
                                    </div>
                                    <div class="row form-group m-form__group m-login__form-sub">
                                        <div class="col m--align-left">
                                            <label class="m-checkbox m-checkbox--focus">
                                                <input type="checkbox" name="agree"> Estoy de acuerdo
                                                <a href="#" class="m-link m-link--focus">
                                                        Terminos y condiciones
                                                    </a>
                                                <span></span>
                                            </label>
                                            <span class="m-form__help"></span>
                                        </div>
                                    </div>
                                    <div class="m-login__form-action">
                                        <button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                            Regístrate
                                        </button>
                                        <button id="m_login_signup_cancel" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                            Cancelar
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="m-login__forget-password">
                                <div class="m-login__head">
                                    <h3 class="m-login__title">
                                           ¿ Olvidaste la contraseña ?
                                        </h3>
                                    <div class="m-login__desc">
                                        Ingrese su correo electrónico para restablecer su contraseña:
                                    </div>
                                </div>
                                <form class="m-login__form m-form" action="">
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="password" placeholder="Contraseña" name="password">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirmar Contraseña" name="rpassword">
                                    </div>
                                    <div class="m-login__form-action">
                                        <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                            Restablecer
                                        </button>
                                        <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                            Cancelar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url(templante/fondo1.png)">
                <div class="m-grid__item m-grid__item--middle">
                    <h3 class="m-login__welcome">
                            iruTv te conecta
                        </h3>
                    <p class="m-login__msg">
                       Descubriendo 
                        <br> nuestra provincia
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Page -->
    <!--begin::Base Scripts -->
    <script src="{{asset('templante/metronic/default/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('templante/metronic/default/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Base Scripts -->
    <!--begin::Page Snippets -->
    <script src="{{asset('templante/metronic/default/assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>

    <!--end::Page Snippets -->
</body>
<!-- end::Body -->

</html>

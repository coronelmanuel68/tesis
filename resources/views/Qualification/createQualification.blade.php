
@extends('master')

@section('stylus')


@endsection
@section('content_admin')

    <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Calificacion
                            </h3>
                        </div>
                    </div>
                </div>
                
            
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('Qualification')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="sistem" value="2">
                    <div class="m-portlet__body">
                    @if (session('typemsg'))
                        @if (session('typemsg') == 'success')
                            <div class="alert alert-success">
                            <strong><p>{{ session('message') }}</p></strong>
                            </div>
                        @endif
                        @if (session('typemsg') == 'error')
                            <div class="alert alert-danger">
                            <strong><p>{{ session('message') }}</p></strong>
                            </div>
                        @endif	
                    @endif		
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                            <label>
                                <strong> Calificaion: </strong> 
                            </label>
                            <input type="text" name="qualification" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Calificacion" @if (old('qualification')) value="{{ old('name') }}" @else value="{{ $Qualification->name }}"@endif>
                            
                            @if ($errors->any())
                                @if($errors->has('name'))
                                <div class="form-control-feedback">
                                    Por favor ingrese una Calificacion
                                </div>
                                @endif
                            @endif
                        </div>
    
                    </div>                  
                        <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                                <label for="exampleSelect1">
                                <strong> Trimestre: </strong>
                                    </label>
                                    <select name="type_of_contact" class="form-control m-input" id="type_of_contact">
                                       
                                        @foreach ($typeOfContacts as $typeOfContact)
                                                <option value="{{ $typeOfContact->id }}" @if ($typeOfContact->id == 1) selected @endif
                                                    @if (old('type_of_contact'))
                                                        @if (old('type_of_contact') == $typeOfContact->id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >
                                                    {{ $typeOfContact->name }}
                                                </option>
                                        @endforeach 
                                            
                                </select>
                              
                             
                                <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                            
                        </div>
                            
                    
                                                  
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('Qualification')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
                <!--end::Form-->
               
    </div>
@endsection


@section('script')

@endsection
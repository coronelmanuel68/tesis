@extends('master')

@section('stylus')


@endsection
@section('content_admin')
        
        
<div class="m-content">

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="la flaticon-user"></i>
                    Calificaciones
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->	
        @if (session('typemsg'))
                @if (session('typemsg') == 'success')
                    <div class="alert alert-success">
                    <strong><p>{{ session('message') }}</p></strong>
                    </div>
                @endif
                @if (session('typemsg') == 'error')
                    <div class="alert alert-danger">
                    <strong><p>{{ session('message') }}</p></strong>
                    </div>
                @endif	
        @endif				
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="Buscar..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
               
               
            </div>
        </div>
        <!--end: Search Form -->
<!--begin: Datatable -->

        <table class="m-datatable" id="table-competitors" width="100%">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellido
                    </th>
                   
                    <th>
                        Curso
                    </th>
                   
                    <th>
                        Acción
                    </th>
                </tr>
            </thead>
            <tbody>
                 @foreach($listQualification as $qualification)
                        <tr>
                            <td>
                                {{ $qualification->people->name }}
                            </td>
                            <td>
                                {{ $qualification->people->lastname }}
                            </td>
                            <td>
                                {{ $qualification->courses->year }}
                            </td>
                            <td>
                            <a
                                
                                    href="{{url('Qualification/'.$qualification->id.'/show')}}"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"
                                    title="Cargar ">
                                    <i class="la la-edit"></i>
                                   
                                </a>
                                <a
                                
                                    href="{{url('Qualification/'.$qualification->id.'/edit')}}"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"
                                    title="Editar ">
                                    <i class="la la-edit"></i>
                                   
                                </a>
                             
                                    <a

                                    
                                        href="{{url('Qualification/'.$qualification->id.'/destroy')}}"
                                        data-id="{{$qualification->student_id}}" 
                                        data-state="{{$qualification->state_id }}" 
                                        data-course="{{$qualification->course_id}}"
                                        data-name="{{$qualification->people->name}}" 
                                        data-lastname="{{$qualification->people->lastname}}"
                                      
                                        class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteStudent"
                                        title="Eliminar">
                                        <i class="la la-trash"></i>
                                    </a>
                                    
                            </td>
                        </tr> 
                      
                @endforeach
            </tbody>
        
        </table>
        <!--end: Datatable -->
    </div>
</div>
</div>


@endsection

@section('script')

		
@endsection
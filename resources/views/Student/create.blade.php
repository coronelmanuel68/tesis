
@extends('master')

@section('stylus')


@endsection
@section('content_admin')

    <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Nuevo Estudiante
                            </h3>
                        </div>
                    </div>
                </div>
                
            
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('Student')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="sistem" value="2">
                    <div class="m-portlet__body">
                    @if (session('typemsg'))
                        @if (session('typemsg') == 'success')
                            <div class="alert alert-success">
                            <strong><p>{{ session('message') }}</p></strong>
                            </div>
                        @endif
                        @if (session('typemsg') == 'error')
                            <div class="alert alert-danger">
                            <strong><p>{{ session('message') }}</p></strong>
                            </div>
                        @endif	
                    @endif		
                    <div class="form-group m-form__group row">
                        <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                            <label>
                                <strong> Nombre: </strong> 
                            </label>
                            <input type="text" name="name" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Nombre" @if (old('name')) value="{{ old('name') }}" @else value="{{ $student->name }}"@endif>
                            
                            @if ($errors->any())
                                @if($errors->has('name'))
                                <div class="form-control-feedback">
                                    Por favor ingrese un nombre
                                </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('lastname')) has-danger @endif">
                            <label>
                            <strong> Apellido: </strong>
                                
                            </label>
                            <input type="text" name="lastname" id="LastName" class="form-control m-input form-control-danger @if($errors->has('lastname')) form-control-danger @endif" placeholder="Apellido" @if (old('lastname')) value="{{ old('lastname') }}" @else value="{{ $student->lastname }}"@endif>
                            @if ($errors->any())
                                @if($errors->has('lastname'))
                                <div class="form-control-feedback">
                                    Por favor ingrese un apellido
                                </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('lastname')) has-danger @endif">
                            <label>
                            <strong> DNI: </strong>
                                
                            </label>
                            <input type="integer" name="dni" id="dni" class="form-control m-input form-control-danger @if($errors->has('dni')) form-control-danger @endif" placeholder="DNI" @if (old('dni')) value="{{ old('dni') }}" @else value="{{ $student->dni }}"@endif>
                            @if ($errors->any())
                                @if($errors->has('dni'))
                                <div class="form-control-feedback">
                                    Por favor ingrese un dni
                                </div>
                                @endif
                            @endif
                        </div>
                        <div class="col-lg-6 @if($errors->has('lastname')) has-danger @endif">
                            <label>
                            <strong> Fecha de Nacimiento: </strong>
                                
                            </label>
                            <input type="date" name="birthdate" id="birthdate" class="form-control m-input form-control-danger @if($errors->has('birthdate')) form-control-danger @endif" placeholder="Fecha de Nacimiento" @if (old('birthdate')) value="{{ old('lastname') }}" @else value="{{ $student->birthdate }}"@endif>
                            @if ($errors->any())
                                @if($errors->has('lastname'))
                                <div class="form-control-feedback">
                                    Por favor ingrese un apellido
                                </div>
                                @endif
                            @endif
                        </div>
                        
                    </div>                  
                        <div class="form-group m-form__group row">
                        <div class="col-lg-6">
                                <label for="exampleSelect1">
                                <strong> Tipo de Contacto: </strong>
                                    </label>
                                    <select name="type_of_contact" class="form-control m-input" id="type_of_contact">
                                       
                                        @foreach ($typeOfContacts as $typeOfContact)
                                                <option value="{{ $typeOfContact->id }}" @if ($typeOfContact->id == 1) selected @endif
                                                    @if (old('type_of_contact'))
                                                        @if (old('type_of_contact') == $typeOfContact->id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >
                                                    {{ $typeOfContact->name }}
                                                </option>
                                        @endforeach 
                                            
                                </select>
                              
                             
                                <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                            
                        </div>
                            </div>
                            
                            <div class="col-lg-6 @if($errors->has('contact')) has-danger @endif">
                                <label>
                                    <strong> Contacto: </strong> 
                                </label>
                                <input type="" name="contact" id="contact" class="form-control m-input @if($errors->has('contact')) form-control-danger @endif" placeholder="Contacto" value="{{ old('contact') }}">
                                @if ($errors->any())
                                    @if($errors->has('contact'))
                                        <div class="form-control-feedback">
                                            Por favor ingrese un tipo de contacto     
                                    
                                        </div>
                                    @endif 
                                    
                                @endif
                                
                            </div>
                            </div>
                     
                      <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label for="exampleSelect1">
                                <strong> Curso: </strong>
                                    
                                    </label>
                                    <select name="course" class="form-control m-input" id="course">
                                       
                                        @foreach ($courses as $course)
                                                <option value="{{ $course->id }}" @if ($course->id == 1) selected @endif
                                                    @if (old('course'))
                                                        @if (old('course') == $course->id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >
                                                    {{ $course->year }}
                                                </option>
                                        @endforeach 
                                            
                                </select>

                            </div>
                            
                        </div>

                       
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('Student')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
                <!--end::Form-->
               
    </div>
@endsection


@section('script')

@endsection
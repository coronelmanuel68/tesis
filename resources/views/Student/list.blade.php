
@extends('master')

@section('stylus')


@endsection
@section('content_admin')
        
        
<div class="m-content">

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="la flaticon-user"></i>
                    Alumnos
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->	
        @if (session('typemsg'))
                @if (session('typemsg') == 'success')
                    <div class="alert alert-success">
                    <strong><p>{{ session('message') }}</p></strong>
                    </div>
                @endif
                @if (session('typemsg') == 'error')
                    <div class="alert alert-danger">
                    <strong><p>{{ session('message') }}</p></strong>
                    </div>
                @endif	
        @endif				
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="Buscar..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span>
                                        <i class="la la-search"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
               
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{url('Student/create')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la flaticon-user-add"></i>
                                <span>
                                    Nuevo
                                </span>
                            </span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
               
            </div>
        </div>
        <!--end: Search Form -->
<!--begin: Datatable -->

        <table class="m-datatable" id="table-competitors" width="100%">
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellido
                    </th>
                    <th>
                       Dni
                    </th>
                    <th>
                        Estado
                    </th>
                    <th>
                        Curso
                    </th>
                   
                    <th>
                        Acción
                    </th>
                </tr>
            </thead>
            <tbody>
                 @foreach($listStudent as $student)
                        <tr>
                            <td>
                                {{ $student->people->name }}
                            </td>
                            <td>
                                {{ $student->people->lastname }}
                            </td>
                            <td>
                                {{ $student->people->dni }}
                            </td>
                            <td>
                                {{ $student->state->name }}
                            </td>
                            <td>
                                {{ $student->courses->year }}
                            </td>
                            <td>
                                <a
                                
                                    href="{{url('Student/'.$student->id.'/edit')}}"
                                    class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateUser"
                                    title="Editar ">
                                    <i class="la la-edit"></i>
                                   
                                </a>
                             
                                    <a

                                    
                                        href="{{url('Student/'.$student->id.'/destroy')}}"
                                        data-id="{{$student->student_id}}" 
                                        data-state="{{$student->state_id }}" 
                                        data-course="{{$student->course_id}}"
                                        data-name="{{$student->people->name}}" 
                                        data-lastname="{{$student->people->lastname}}"
                                        data-dni="{{$student->people->dni}}"
                                        data-birthdate="{{$student->people->birthdate}}"
                                        
                                        class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteStudent"
                                        title="Eliminar">
                                        <i class="la la-trash"></i>
                                    </a>
                                    
                            </td>
                        </tr> 
                      
                @endforeach
            </tbody>
        
        </table>
        <!--end: Datatable -->
    </div>
</div>
</div>


@endsection

@section('script')

		
@endsection
//== Class definition

var DatatableHtmlTableDemo = function() {
  //== Private functions

  // demo initializer
  var demo = function() {

    var datatable = $('.m-datatable').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo();
    },
  };
}();

var DatatableHtmlTableDemo2 = function() {
  //== Private functions

  // demo initializer
  var demo2 = function() {

    var datatable = $('.m-datatable2').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch2'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo2();
    },
  };
}();

var DatatableHtmlTableDemo3 = function() {
  //== Private functions

  // demo initializer
  var demo3 = function() {

    var datatable = $('.m-datatable3').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch3'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo3();
    },
  };
}();


var DatatableHtmlTableDemo4 = function() {
  //== Private functions

  // demo initializer
  var demo4 = function() {

    var datatable = $('.m-datatable4').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch4'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo4();
    },
  };
}();

var DatatableHtmlTableDemo5 = function() {
  //== Private functions

  // demo initializer
  var demo5 = function() {

    var datatable = $('.m-datatable5').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch5'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo5();
    },
  };
}();

var DatatableHtmlTableDemo6 = function() {
  //== Private functions

  // demo initializer
  var demo6 = function() {

    var datatable = $('.m-datatable6').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch6'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo6();
    },
  };
}();

jQuery(document).ready(function() {
  DatatableHtmlTableDemo.init();
  DatatableHtmlTableDemo2.init();
  DatatableHtmlTableDemo3.init();
  DatatableHtmlTableDemo4.init();
  DatatableHtmlTableDemo5.init();
  DatatableHtmlTableDemo6.init();
});
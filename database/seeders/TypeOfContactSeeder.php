<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOfContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_contacts')->insert([
            'name' => 'Telefono',
            
        ]);
       
        DB::table('type_of_contacts')->insert([
            'name' => 'Email',
            
        ]);

        DB::table('type_of_contacts')->insert([
            'name' => 'Red Social',
            
        ]);
    }
}

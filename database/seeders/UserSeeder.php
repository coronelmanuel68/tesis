<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('users')->insert([
            'email' => 'superUser@gmail.com',
            'password' => Hash::make('1234'),
            'rol_id' => 1,
            'people_id' => 1,
            

        ]);

        DB::table('users')->insert([
            'email' => 'admin@gmail.com',
            'password' => Hash::make('1234'),
            'rol_id' => 2,
            'people_id' => 2,

        ]);

        DB::table('users')->insert([
            'email' => 'editor@gmail.com',
            'password' => Hash::make('1234'),
            'rol_id' => 3,
            'people_id' => 3,

        ]);
        
    }
}

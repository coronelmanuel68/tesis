<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'name' => 'superUser',
            'lastname' => 'superUser',
            'dni' =>'23465789',
            'birthdate' =>'1991/11/12'

        ]);
        
        DB::table('people')->insert([
            'name' => 'pedro',
            'lastname' => 'quiñones',
            'dni' =>'56789098',
            'birthdate' =>'1991/10/12'

        ]);
    
        DB::table('people')->insert([
            'name' => 'juan',
            'lastname' => 'coronel',
            'dni' =>'12345876',
            'birthdate' =>'1991/8/12'

        ]);
    }



}
